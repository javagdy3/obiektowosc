package pl.sda.jgdy5.obiektowosc.main;

import pl.sda.jgdy5.obiektowosc.KontoBankowe;

public class MainKontoBankowe {
    public static void main(String[] args) {
        KontoBankowe kontoAndrzeja = new KontoBankowe(123L, 1000);
        KontoBankowe kontoBeaty = new KontoBankowe(555L, 2000);

        kontoBeaty.wplacSrodki(kontoAndrzeja.pobierzSrodki(700));
        kontoBeaty.wplacSrodki(kontoAndrzeja.pobierzSrodki(500));

        kontoBeaty.wyswietlStanKonta();
        kontoAndrzeja.wyswietlStanKonta();

//        kontoBeaty.stanKonta = 0;
    }
}
