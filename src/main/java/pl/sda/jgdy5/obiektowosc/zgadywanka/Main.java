package pl.sda.jgdy5.obiektowosc.zgadywanka;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random generator = new Random();

        int licznikOdpowiedzi = 5;
        System.out.println("Podaj zakres liczbowy do odgadnięcia:");
        int zakres = scanner.nextInt();
        int losowaLiczba = generator.nextInt(zakres);

        int odpowiedź; // odpowiedź użytkownika

        do {
            System.out.println("Pozostało Ci " + licznikOdpowiedzi + " szans.");
            System.out.println("Odgadnij liczbę:"); //na początku do-while wypisz użytkownikowi  komunikat "odgadnij liczbę"
            odpowiedź = scanner.nextInt(); //po wyświetleniu komunikatu użyj obiektu scannera do wczytania liczby do zmiennej (wcześniej zadeklarowanej) int odpowiedź;

            if (odpowiedź > losowaLiczba) {
                System.out.println("za duża");
            } else if (odpowiedź < losowaLiczba) {
                System.out.println("za mała");
            } else {
                System.out.println("al dente");
                System.out.println("Gratulacje!");
                break;
            }

            licznikOdpowiedzi--;
        } while (licznikOdpowiedzi > 0); //stwórz pętlę do-while. Warunkiem końcowym pętli ma być (licznikOdpowiedzi<5)
    }
}
