package pl.sda.jgdy5.obiektowosc.dziedziczenie_po_object;

import pl.sda.jgdy5.obiektowosc.dziedziczenie.Bocian;
import pl.sda.jgdy5.obiektowosc.dziedziczenie.Kukulka;

public class Main {
    public static void main(String[] args) {
        Bocian bocian1 = new Bocian();
        Bocian bocian2 = new Bocian();
        bocian1.identyfikator = "5";
        bocian2.identyfikator = "5";

        if(bocian1.equals(bocian2)){
            System.out.println("Te same boćki");
        }
    }
}
