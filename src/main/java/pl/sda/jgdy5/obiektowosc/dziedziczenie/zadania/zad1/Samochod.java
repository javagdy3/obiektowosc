package pl.sda.jgdy5.obiektowosc.dziedziczenie.zadania.zad1;

import java.util.Objects;

public class Samochod {
    protected String kolor, marka;
    protected int rocznik;

    protected int predkosc;
    private boolean swiatlaWlaczone;

    public Samochod() {
    }

    public Samochod(String kolor, String marka, int rocznik) {
        this.kolor = kolor;
        this.marka = marka;
        this.rocznik = rocznik;
    }

    public void przyspiesz(int oIle){
        // może istnieć wiele metod o tej samej nazwie ale innej ilości parametrów
        // lub o innym typie parametrów
    }

    public void przyspiesz() {
        if (predkosc <= 110) {
            predkosc += 10;
            System.out.println("Przyspieszam do " + predkosc + " km/h");
        }
    }

    public void wlaczSwiatla() {
        swiatlaWlaczone = true;
    }

    public boolean czySwiatlaWlaczone() {
        return swiatlaWlaczone;
    }

    @Override
    public String toString() {
        return kolor + " samochód marki " + marka + " rocznik " + rocznik;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Samochod samochod = (Samochod) o;
        return rocznik == samochod.rocznik &&
                Objects.equals(kolor, samochod.kolor) &&
                Objects.equals(marka, samochod.marka);
    }

    @Override
    public int hashCode() {
        return Objects.hash(kolor, marka, rocznik);
    }
}
