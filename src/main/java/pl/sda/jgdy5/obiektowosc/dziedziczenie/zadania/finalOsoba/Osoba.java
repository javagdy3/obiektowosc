package pl.sda.jgdy5.obiektowosc.dziedziczenie.zadania.finalOsoba;

public class Osoba {
    public String imie;
    public int rocznik;

    public Osoba(String imie, int rocznik) {
        this.imie = imie;
        this.rocznik = rocznik;
    }
}

