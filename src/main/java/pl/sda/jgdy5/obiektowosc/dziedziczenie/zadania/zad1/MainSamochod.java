package pl.sda.jgdy5.obiektowosc.dziedziczenie.zadania.zad1;

public class MainSamochod {
    public static void main(String[] args) {
        Samochod samochod = new Samochod("zielony", "Fiat 126p", 1990);
        samochod.przyspiesz();

        for (int i = 0; i < 30; i++) {
            samochod.przyspiesz();
        }

        samochod.wlaczSwiatla();
        if (samochod.czySwiatlaWlaczone()) {
            System.out.println("Światła włączone");
        }
//        ################################################
        System.out.println("Test kabrioletu");
        Kabriolet kabriolet = new Kabriolet("Czerwony", "Ferrari", 1985);
        kabriolet.przyspiesz();

        for (int i = 0; i < 30; i++) {
            kabriolet.przyspiesz();
        }

        kabriolet.wlaczSwiatla();
        if (kabriolet.czySwiatlaWlaczone()) {
            System.out.println("Światła włączone");
        }

        if (kabriolet.czyDachSchowany()) {
            System.out.println("Tak, schowany");
        } else {
            System.out.println("Nie jest");
        }

        kabriolet.schowajDach();

        if (kabriolet.czyDachSchowany()) {
            System.out.println("Tak, jest schowany");
        } else {
            System.out.println("Nie jest");
        }

        System.out.println(samochod);

//        System.out.println(kabriolet.toString());
        System.out.println("Acotusiewypisze: " + kabriolet);

        Samochod doPorownania = new Samochod("zielony", "Fiat 126p", 1990);
        if(samochod.equals(doPorownania)){
            System.out.println("Wow, pacz tato taki sam jak nasz...");
        }
    }
}
