package pl.sda.jgdy5.obiektowosc.dziedziczenie.zadania.zad2;

public class Biblioteka {
    Egzemplarz[] zbiorEgzemplarzy;

    Egzemplarz[] szukajPoTytule(String tytul) {
        int iloscPasujacych = 0;
        // szukamy ilosci, zeby móc stworzyć podtabelę (potrzebny jest rozmiar)
        for (int i = 0; i < zbiorEgzemplarzy.length; i++) {
            if (zbiorEgzemplarzy[i].tytul.equals(tytul)) {
                iloscPasujacych++;
            }
        }
        // właśnie zliczyliśmy ilość pasujących egzemplarzy.
        Egzemplarz[] znalezione = new Egzemplarz[iloscPasujacych]; // 3 [0,1,2]

        int miejsceWstawiania = 0;
        for (int i = 0; i < zbiorEgzemplarzy.length; i++) { // 10, 15, 30
            if (zbiorEgzemplarzy[i].tytul.equals(tytul)) {
                // trzeba wstawić książkę pasującą na opowiednią pozycję w znalezionych
                znalezione[miejsceWstawiania++] = zbiorEgzemplarzy[i];
            }
        }

        return znalezione;
    }

    Egzemplarz[] szukajPoAutorze(String autor) {
        int iloscPasujacych = 0;
        // szukamy ilosci, zeby móc stworzyć podtabelę (potrzebny jest rozmiar)
        for (int i = 0; i < zbiorEgzemplarzy.length; i++) {
            if (zbiorEgzemplarzy[i].autor.nazwisko.equals(autor)) {
                iloscPasujacych++;
            }
        }
        // właśnie zliczyliśmy ilość pasujących egzemplarzy.
        Egzemplarz[] znalezione = new Egzemplarz[iloscPasujacych]; // 3 [0,1,2]

        int miejsceWstawiania = 0;
        for (int i = 0; i < zbiorEgzemplarzy.length; i++) { // 10, 15, 30
            if (zbiorEgzemplarzy[i].autor.nazwisko.equals(autor)) {
                // trzeba wstawić książkę pasującą na opowiednią pozycję w znalezionych
                znalezione[miejsceWstawiania++] = zbiorEgzemplarzy[i];
            }
        }

        return znalezione;
    }

    Egzemplarz[] szukajPoAutorzeLubTytule(String fraza) {
        Egzemplarz[] poTytule = szukajPoTytule(fraza);
        Egzemplarz[] poAutorze = szukajPoAutorze(fraza);

        Egzemplarz[] suma = new Egzemplarz[poAutorze.length + poTytule.length];
        int i = 0;
        for (; i < poTytule.length; i++) {
            suma[i] = poTytule[i];
        }

        for (int j = 0; j < poAutorze.length; j++) {
            suma[i++] = poAutorze[j];
        }
        return suma;
    }
}
