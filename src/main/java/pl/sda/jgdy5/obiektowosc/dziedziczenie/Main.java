package pl.sda.jgdy5.obiektowosc.dziedziczenie;

public class Main {
    public static void main(String[] args) {
//        Ptak p = new Ptak(); // do zmiennej ptak przypisujemy obiekt ptak
//        p.spiewaj(); // zaśpiewa ćwir ćwir

        // do zmiennej kukulka przypisujemy sobie obiekt kukułka
        Kukulka kukulka = new Kukulka();
//        kukulka.spiewaj(); // zaśpiewa kuku
//
        Ptak ptaszek = new Sowa(); // do zmiennej PTAK przypisuję SOWA
//        ptaszek.spiewaj(); // ponieważ jest to referencja typu SOWA (taki obiekt
        // przypisaliśmy do zmiennej), to metoda zaśpiewa "hu hu" z klasy sowa

        Ptak[] pt = new Ptak[4]; // tablica, 4 elementy - ale pusta
        // dzięki dziedziczeniu możemy uwspólnić i wstawić obiekty które dziedziczą
        // po pewnym typie do jednej tablicy/listy/kontenera
        pt[0] = new Sowa();
        pt[1] = kukulka;
        pt[2] = ptaszek;
        pt[3] = new Orzeł();

        // pętla wypisująca
        for (int i = 0; i < pt.length; i++) {
            pt[i].spiewaj();

            // * możemy sprawdzić czy obiekt jest danego typu (instanceof)
            if (pt[i] instanceof Kukulka) { // upewnienie się przez NAS że obiekt jest Kukułką

                // rzutujemy obiekt na inny typ
                Kukulka k = (Kukulka) pt[i]; // mówię: "Ty, pt[i] (ptak), Ty jesteś Kukułka"
                k.zmusLudziDoWyciaganiaHajsu();
                System.out.println("To jest kukułka");
            }
        }
    }
}
