package pl.sda.jgdy5.obiektowosc.dziedziczenie;

import java.util.Objects;

// klasa Bocian
public class Bocian extends Ptak{
    public String identyfikator;

    public void spiewaj(){
        System.out.println("kle kle");
    }
    public void przyniesLudziomDzieci(){
        System.out.println("hi hi kle kle");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        // jeśli porównanie "czy są tych samych klas" jest false, to kończę
        // to nie ma sensu porównywać Kukułki do Bociana
        if (o == null || getClass() != o.getClass()) return false;

        // rzutuję obiekt porównywany do bociana
        Bocian bocian = (Bocian) o;
        return Objects.equals(identyfikator, bocian.identyfikator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identyfikator);
    }
}
