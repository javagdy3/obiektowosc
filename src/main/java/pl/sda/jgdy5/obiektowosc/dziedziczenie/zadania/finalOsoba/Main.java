package pl.sda.jgdy5.obiektowosc.dziedziczenie.zadania.finalOsoba;

public class Main {
    public static void main(String[] args) {
        final Osoba o = new Osoba("Pawel", 1990); // #333
        int zmienna = 5;                                // #444

        zmienna = 6;
//        o = new Osoba("Gawel", 1995); // #456
        o.imie = "Gawel";

    }
}
