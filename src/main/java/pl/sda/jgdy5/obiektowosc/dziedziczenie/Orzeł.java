package pl.sda.jgdy5.obiektowosc.dziedziczenie;

public class Orzeł extends Ptak {
    @Override
    public void spiewaj() {
        super.spiewaj(); // wypisze się 'ćwir ćwir' z klasy Ptak
        System.out.println("orły nie robią ćwir ćwir");
    }
}
