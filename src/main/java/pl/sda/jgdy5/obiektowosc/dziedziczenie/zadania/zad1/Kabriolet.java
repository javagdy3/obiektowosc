package pl.sda.jgdy5.obiektowosc.dziedziczenie.zadania.zad1;

public class Kabriolet extends Samochod {
    private boolean dachSchowany;

    public Kabriolet(String kolor, String marka, int rocznik) {
        super(kolor, marka, rocznik);
    }

    @Override
    public void przyspiesz() {
        super.przyspiesz();
        if (predkosc >= 120 && predkosc <= 170) {
            predkosc += 10;
            System.out.println("Przyspieszam do " + predkosc + " km/h");
        }
    }

    public void schowajDach() {
        if (dachSchowany) {
            System.out.println("Dach jest już schowany");
        } else {
            dachSchowany = true;
        }
    }

    public boolean czyDachSchowany() {
        return dachSchowany;
    }

//    @Override
//    public String toString() {
//        return "Kabriolet{" +
//                "kolor='" + kolor + '\'' +
//                ", marka='" + marka + '\'' +
//                ", rocznik=" + rocznik +
//                '}';
//    }
}
