package pl.sda.jgdy5.obiektowosc.enumerated;

public enum Bilet {
    ULGOWY(2.10),
    NORMALNY(4.20),
    CAŁODOBOWY(20.0),
    MIESIĘCZNY(400.0);

    private final double cena;

    Bilet(double cena) {
        this.cena = cena;
    }

    public double getCena() {
        return cena;
    }
}
