package pl.sda.jgdy5.obiektowosc.enumerated;

public enum Płeć {
    KOBIETA,
    MĘŻCZYZNA;
}
