package pl.sda.jgdy5.obiektowosc.enumerated;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String rodzajBiletu = scanner.next();


//        Bilet bilecior = Bilet.NORMALNY;
        Bilet bilecior = Bilet.valueOf(rodzajBiletu.toUpperCase().trim());

//        switch (bilecior) {
//            case ULGOWY:
//                System.out.println("Koszt 2.40");
//                break;
//            case NORMALNY:
//                System.out.println("Koszt 4.20");
//                break;
//            case CAŁODOBOWY:
//                System.out.println("Koszt 20.0");
//                break;
//            case MIESIĘCZNY:
//                System.out.println("Koszt 300.0");
//                break;
//            default:
//                System.out.println("A co to takiego?");
//        }

        System.out.println(bilecior + " cena = " + bilecior.getCena());
    }
}
