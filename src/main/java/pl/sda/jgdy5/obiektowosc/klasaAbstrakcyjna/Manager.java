package pl.sda.jgdy5.obiektowosc.klasaAbstrakcyjna;

public class Manager extends Osoba {
    public Manager(String name) {
        super(name);
    }

    @Override
    public void podajSwojeStanowisko() {
        System.out.println("Jestem managerem.");
    }

    @Override
    public boolean czyJestesManagerem() {
        return true;
    }
}
