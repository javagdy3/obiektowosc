package pl.sda.jgdy5.obiektowosc.klasaAbstrakcyjna;

public class Pracownik extends Osoba {
    public Pracownik(String name) {
        super(name);
    }

    @Override
    public void podajSwojeStanowisko() {
        System.out.println("Jestem pracownikiem");
    }

    @Override
    public boolean czyJestesManagerem() {
        return false;
    }
}
