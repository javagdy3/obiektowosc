package pl.sda.jgdy5.obiektowosc.klasaAbstrakcyjna;

public class Main {
    public static void main(String[] args) {
        Osoba osoba = new Pracownik("Marian");
        Osoba osoba2 = new Manager("Lucjan");

        // nie może istnieć instancja klasy abstrakcyjnej
//        Osoba osoba3 = new Osoba();

        Osoba[] p = new Osoba[2];
        p[0] = osoba;
        p[1] = osoba2;
        int sumaManagerow = 0;

        for (int i = 0; i < p.length; i++) {
            p[i].podajSwojeStanowisko();
            if(p[i].czyJestesManagerem()){
                sumaManagerow++;
            }
        }

        System.out.println("Ilosc managerow: " + sumaManagerow);

    }
}
