package pl.sda.jgdy5.obiektowosc.klasaAbstrakcyjna;

public abstract class Osoba {
    protected String name;

    public Osoba(String name) {
        this.name = name;
    }

    public void przedstawSie() {
        System.out.println("Siema, jestem " + name);
    }

    public abstract void podajSwojeStanowisko();
    public abstract boolean czyJestesManagerem();

}
