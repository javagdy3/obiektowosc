package pl.sda.jgdy5.obiektowosc;

public class KontoBankowe {
    private long numerKonta;
    private int stanKonta;

    public KontoBankowe(long numerKonta, int stanKonta) {
        this.numerKonta = numerKonta;
        this.stanKonta = stanKonta;
    }

    public void wyswietlStanKonta() {
        System.out.println("Stan konta: " + stanKonta);
    }

    public void wplacSrodki(int ileSrodkow) {
        stanKonta += ileSrodkow;
    }

    public int pobierzSrodki(int ileSrodkow) {
        if (ileSrodkow <= stanKonta) {
            stanKonta -= ileSrodkow;
            return ileSrodkow;
        } else {
            System.out.println("Nie masz dostatecznie dużo środków na koncie.");
            wyswietlStanKonta();
//            ileSrodkow = stanKonta;
//            stanKonta = 0;
//            return ileSrodkow;
            return 0;
        }
    }
}
