package pl.sda.jgdy5.obiektowosc;

public class Main {
    public static void main(String[] args) {
        Osoba osoba_ania = new Osoba("Ania", 1995);
        osoba_ania.przedstawSie();

        Osoba osobaAndrzej = new Osoba();
//        osobaAndrzej.imie = "Andrzej";
        osobaAndrzej.setImie("Andrzej");
//        osobaAndrzej.rokUrodzenia = 1966;
        osobaAndrzej.setRokUrodzenia(1966);
        osobaAndrzej.przedstawSie();

        Osoba mariola = new Osoba("Mariola", 1952);
        mariola.przedstawSie();

        System.out.println();
        Osoba[] tablica = new Osoba[3];
        tablica[0] = osoba_ania;
        tablica[1] = osobaAndrzej;
        tablica[2] = mariola;

//        Alternatywnie, w jednej linii:
//        Osoba[] tablica = new Osoba[]{osoba_ania, osobaAndrzej, mariola};
        for (int i = 0; i < tablica.length; i++) {
            tablica[i].przedstawSie();
        }
//        KontoBankowe k = new KontoBankowe(1L, 100);
//        k.stanKonta = 100000;
    }
}
