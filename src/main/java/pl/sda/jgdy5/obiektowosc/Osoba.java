package pl.sda.jgdy5.obiektowosc;

import pl.sda.jgdy5.obiektowosc.enumerated.Bilet;

public class Osoba {
    private String imie;
    private int rokUrodzenia;


    public Osoba() {
    }

    public Osoba(String imie1, int rokUrodzenia) {
        // ten obiekt, imie, ustaw imię z parametru.
        imie = imie1;
        this.rokUrodzenia = rokUrodzenia;
    }

    public void setImie(String noweImie){
        this.imie = noweImie;
    }

    public void setRokUrodzenia(int rokUrodzenia) {
        this.rokUrodzenia = rokUrodzenia;
    }

    void przedstawSie() {
        System.out.println("Cześć! Mam na imię " + imie + " i mam " + (2020 - rokUrodzenia) + " lat.");
    }
}
